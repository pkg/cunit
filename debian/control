Source: cunit
Priority: optional
Maintainer: Azat Khuzhin <a3at.mail@gmail.com>
Build-Depends: debhelper (>= 9), dh-autoreconf, libncurses-dev
Standards-Version: 3.9.6
Section: libs
Vcs-Git: https://salsa.debian.org/debian/cunit.git
Vcs-Browser: https://salsa.debian.org/debian/cunit
Homepage: http://cunit.sourceforge.net/
Rules-Requires-Root: no

Package: libcunit1-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Suggests: libcunit1-doc (= ${binary:Version})
Depends: ${misc:Depends}, libcunit1 (= ${binary:Version})
Description: Unit Testing Library for C -- development files
 CUnit is a simple framework for incorporating test cases in your C
 code similar to JUnit or CppUnit. It provides C programmers a basic
 testing functionality with a flexible variety of user interfaces.  It
 uses a simple framework for building test structures, and provides a
 rich set of assertions for testing common data types.  In addition,
 several different interfaces are provided for running tests and
 reporting results.
 .
 This package includes development files for compiling against cunit.

Package: libcunit1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Unit Testing Library for C
 CUnit is a simple framework for incorporating test cases in your C
 code similar to JUnit or CppUnit. It provides C programmers a basic
 testing functionality with a flexible variety of user interfaces.  It
 uses a simple framework for building test structures, and provides a
 rich set of assertions for testing common data types.  In addition,
 several different interfaces are provided for running tests and
 reporting results.

Package: libcunit1-ncurses-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Conflicts: libcunit1-dev
Suggests: libcunit1-doc (= ${binary:Version})
Depends: ${misc:Depends}, libcunit1-ncurses (= ${binary:Version})
Description: Unit Testing Library for C (ncurses) -- development files
 CUnit is a simple framework for incorporating test cases in your C
 code similar to JUnit or CppUnit. It provides C programmers a basic
 testing functionality with a flexible variety of user interfaces.  It
 uses a simple framework for building test structures, and provides a
 rich set of assertions for testing common data types.  In addition,
 several different interfaces are provided for running tests and
 reporting results.
 .
 This package includes development files for compiling against cunit (ncurses).

Package: libcunit1-ncurses
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Conflicts: libcunit1
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Unit Testing Library for C (ncurses)
 CUnit is a simple framework for incorporating test cases in your C
 code similar to JUnit or CppUnit. It provides C programmers a basic
 testing functionality with a flexible variety of user interfaces.  It
 uses a simple framework for building test structures, and provides a
 rich set of assertions for testing common data types.  In addition,
 several different interfaces are provided for running tests and
 reporting results.
 .
 This versions includes CUnit curses interface.

Package: libcunit1-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Unit Testing Library for C -- documentation
 CUnit is a simple framework for incorporating test cases in your C
 code similar to JUnit or CppUnit. It provides C programmers a basic
 testing functionality with a flexible variety of user interfaces.  It
 uses a simple framework for building test structures, and provides a
 rich set of assertions for testing common data types.  In addition,
 several different interfaces are provided for running tests and
 reporting results.
 .
 This package contains documentation.
